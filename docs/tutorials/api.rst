API References
==============

idinn.sourcing\_model
---------------------

.. automodule:: idinn.sourcing_model
   :members:
   :undoc-members:
   :show-inheritance:

idinn.controller
----------------

.. automodule:: idinn.controller
   :members:
   :undoc-members:
   :show-inheritance:

idinn.demand
----------------

.. automodule:: idinn.demand
   :members:
   :undoc-members:
   :show-inheritance: