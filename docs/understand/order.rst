What is Behind the Order Process of the Sourcing Models?
========================================================

You might wonder how does the order process and the market demand work in the sourcing models. The order process and the market demand works the same for all the sourcing models. The only difference is how to keep track of the order from different sources.